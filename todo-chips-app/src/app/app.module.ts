import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { EintraegeComponent } from './eintraege/eintraege.component';
import * as fromEintraege from './eintraege/+state/eintraege.reducer';
import { EintraegeEffects } from './eintraege/+state/eintraege.effects';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatChipsModule } from '@angular/material/chips';
import { MatButtonModule } from '@angular/material/button';
import { MatDialogModule } from '@angular/material/dialog';
import { MatInputModule } from '@angular/material/input';
import { MatCheckboxModule } from '@angular/material/checkbox';
import {
  MatDatepickerModule,
  MatDatepickerIntl,
} from '@angular/material/datepicker';
import {
  MatNativeDateModule,
  MAT_DATE_LOCALE,
  MAT_DATE_FORMATS,
  DateAdapter,
} from '@angular/material/core';
import { ReactiveFormsModule } from '@angular/forms';
import { EintragDialogFormComponent } from './eintraege/eintrag-dialog-form/eintrag-dialog-form.component';
import {
  DATE_PICKER_FORMAT,
  getGermanDatepickerIntl,
} from './german-datepicker-intl';
import { DateFnsDatepickerAdapter } from './date-fns-datepicker.adapter';

@NgModule({
  declarations: [AppComponent, EintraegeComponent, EintragDialogFormComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    FlexLayoutModule,
    MatChipsModule,
    MatButtonModule,
    MatDialogModule,
    MatInputModule,
    MatCheckboxModule,
    MatDatepickerModule,
    MatNativeDateModule,
    StoreModule.forRoot(
      {},
      {
        metaReducers: [],
        runtimeChecks: {
          strictStateImmutability: true,
          strictStateSerializability: true,
        },
      }
    ),
    EffectsModule.forRoot([]),
    EffectsModule.forFeature([EintraegeEffects]),
    StoreModule.forFeature(
      fromEintraege.EINTRAEGE_FEATURE_KEY,
      fromEintraege.reducer
    ),
  ],
  providers: [
    { provide: MatDatepickerIntl, useValue: getGermanDatepickerIntl() },
    {
      provide: DateAdapter,
      useClass: DateFnsDatepickerAdapter,
      deps: [MAT_DATE_LOCALE],
    },
    { provide: MAT_DATE_FORMATS, useValue: DATE_PICKER_FORMAT },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
