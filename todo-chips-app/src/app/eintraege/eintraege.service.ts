import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { mockEintraege } from './eintraege.mock';
import { Eintrag, NewEintrag } from './eintraege.models';

@Injectable({
  providedIn: 'root',
})
export class EintraegeService {
  getEintraege(): Observable<Eintrag[]> {
    const storedEintraege = localStorage.getItem('eintraege');
    if (!!storedEintraege) {
      return of(JSON.parse(storedEintraege));
    }

    return of(mockEintraege);
  }

  addNewEintrag(
    eintrag: NewEintrag,
    eintraege: Eintrag[]
  ): Observable<Eintrag[]> {
    const eintragIds = eintraege.map((eintrag) => eintrag.id);
    const newEintrag = {
      ...eintrag,
      id: Math.max(...eintragIds) + 1,
    };
    localStorage.setItem(
      'eintraege',
      JSON.stringify([...eintraege, newEintrag])
    );

    return of([...eintraege, newEintrag]);
  }

  editExistingEintrag(
    updatedEintrag: Eintrag,
    eintraege: Eintrag[]
  ): Observable<Eintrag[]> {
    const updatedEintraege = eintraege.map((eintrag) => {
      if (eintrag?.id !== updatedEintrag?.id) {
        return eintrag;
      }

      return updatedEintrag;
    });
    localStorage.setItem('eintraege', JSON.stringify(updatedEintraege));
    return of(updatedEintraege);
  }

  deleteEintrag(
    eintragId: number,
    eintraege: Eintrag[]
  ): Observable<Eintrag[]> {
    const updatedEintraege = eintraege.reduce((acc, eintrag) => {
      if (eintrag.id !== eintragId) {
        return [...acc, eintrag];
      }

      return acc;
    }, [] as Eintrag[]);
    localStorage.setItem('eintraege', JSON.stringify(updatedEintraege));
    return of(updatedEintraege);
  }
}
