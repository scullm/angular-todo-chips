export interface Eintrag {
  id: number;
  beschreibung: string;
  status: EintragStatus;
  faelligkeitsDatum: Date;
}

export interface NewEintrag {
  beschreibung: string;
  status: EintragStatus;
  faelligkeitsDatum: Date;
}

export type EintragStatus =
  | EintragStatusOptionsEnum.erledigt
  | EintragStatusOptionsEnum.offen;

export enum EintragStatusOptionsEnum {
  'erledigt' = 'erledigt',
  'offen' = 'offen',
}

export interface EintragFormDialogData {
  headingText: string;
  dialogMode: EintragDialogMode;
  eintrag?: Eintrag;
}

export interface EditEintragFormDialogData extends EintragFormDialogData {}

export type EintragDialogMode =
  | EintragDialogModeOptionsEnum.addEintrag
  | EintragDialogModeOptionsEnum.editEintrag;

export enum EintragDialogModeOptionsEnum {
  'addEintrag' = 'add-eintrag',
  'editEintrag' = 'edit-eintrag',
}
