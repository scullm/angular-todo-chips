import { Eintrag, EintragStatusOptionsEnum } from './eintraege.models';

export const mockEintraege: Eintrag[] = [
  {
    id: 0,
    beschreibung: 'mockBeschreibung',
    status: EintragStatusOptionsEnum.offen,
    faelligkeitsDatum: new Date(2021, 1, 10),
  },
  {
    id: 1,
    beschreibung: 'mockBeschreibung',
    status: EintragStatusOptionsEnum.erledigt,
    faelligkeitsDatum: new Date(2021, 1, 5),
  },
  {
    id: 2,
    beschreibung: 'mockBeschreibung',
    status: EintragStatusOptionsEnum.offen,
    faelligkeitsDatum: new Date(2021, 1, 10),
  },
  {
    id: 3,
    beschreibung: 'mockBeschreibung',
    status: EintragStatusOptionsEnum.erledigt,
    faelligkeitsDatum: new Date(2021, 1, 5),
  },
  {
    id: 4,
    beschreibung: 'mockBeschreibung',
    status: EintragStatusOptionsEnum.offen,
    faelligkeitsDatum: new Date(2021, 1, 10),
  },
  {
    id: 5,
    beschreibung: 'mockBeschreibung',
    status: EintragStatusOptionsEnum.erledigt,
    faelligkeitsDatum: new Date(2021, 1, 5),
  },
  {
    id: 6,
    beschreibung: 'mockBeschreibung',
    status: EintragStatusOptionsEnum.offen,
    faelligkeitsDatum: new Date(2021, 1, 10),
  },
  {
    id: 7,
    beschreibung: 'mockBeschreibung',
    status: EintragStatusOptionsEnum.erledigt,
    faelligkeitsDatum: new Date(2021, 1, 5),
  },
  {
    id: 8,
    beschreibung: 'mockBeschreibung',
    status: EintragStatusOptionsEnum.offen,
    faelligkeitsDatum: new Date(2021, 1, 10),
  },
  {
    id: 9,
    beschreibung: 'mockBeschreibung',
    status: EintragStatusOptionsEnum.erledigt,
    faelligkeitsDatum: new Date(2021, 1, 5),
  },
];
