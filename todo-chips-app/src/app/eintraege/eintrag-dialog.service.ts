import { Injectable } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import {
  EintragFormDialogData,
  EditEintragFormDialogData,
} from './eintraege.models';
import { EintragDialogFormComponent } from './eintrag-dialog-form/eintrag-dialog-form.component';

@Injectable({
  providedIn: 'root',
})
export class EintragDialogService {
  constructor(readonly matDialog: MatDialog) {}

  public openDialog(
    data: EintragFormDialogData | EditEintragFormDialogData
  ): MatDialogRef<EintragDialogFormComponent> {
    return this.matDialog.open(EintragDialogFormComponent, {
      width: '',
      maxWidth: '90%',
      height: '',
      data,
      maxHeight: '90%',
      disableClose: false,
      autoFocus: false,
    });
  }
}
