import { Component, Inject } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { EintraegeFacade } from '../+state/eintraege.facade';
import {
  EditEintragFormDialogData,
  Eintrag,
  NewEintrag,
  EintragDialogModeOptionsEnum,
  EintragFormDialogData,
  EintragStatusOptionsEnum,
} from '../eintraege.models';
import { format } from 'date-fns';

@Component({
  selector: 'app-eintrag-dialog-form',
  templateUrl: 'eintrag-dialog-form.component.html',
})
export class EintragDialogFormComponent {
  editEintragForm: FormGroup = this.resolveForm();
  EintragDialogModeOptionsEnum = EintragDialogModeOptionsEnum;

  constructor(
    private readonly eintraegeFacade: EintraegeFacade,
    private readonly formBuilder: FormBuilder,
    readonly matDialogRef: MatDialogRef<EintragDialogFormComponent>,
    @Inject(MAT_DIALOG_DATA)
    public data: EintragFormDialogData | EditEintragFormDialogData
  ) {}

  addNewEintrag() {
    const newEintrag = this.resolveEintragFromForm();

    this.eintraegeFacade.addNewEintrag(newEintrag as NewEintrag);
    this.matDialogRef.close();
  }

  editExistingEintrag() {
    const updatedEintrag = this.resolveEintragFromForm();
    this.eintraegeFacade.editExistingEintrag(updatedEintrag as Eintrag);
    this.matDialogRef.close();
  }

  deleteExistingEintrag() {
    this.eintraegeFacade.deleteEintrag(
      (this.data as EditEintragFormDialogData).eintrag!.id
    );
    this.matDialogRef.close();
  }

  private resolveForm(): FormGroup {
    if (this.data.dialogMode === EintragDialogModeOptionsEnum.editEintrag) {
      return (this.editEintragForm = this.resolveFormGroupFromExistingEintrag());
    }

    return (this.editEintragForm = this.resolveEmptyFormGroup());
  }

  private resolveEmptyFormGroup(): FormGroup {
    return this.formBuilder.group({
      beschreibung: new FormControl(''),
      isStatusErledigt: new FormControl(false),
      faelligkeitsDatum: new FormControl(''),
    });
  }

  private resolveFormGroupFromExistingEintrag(): FormGroup {
    const isStatusErledigt =
      this.data.eintrag?.status === EintragStatusOptionsEnum.erledigt;

    return this.formBuilder.group({
      beschreibung: new FormControl(this.data.eintrag?.beschreibung),
      isStatusErledigt: new FormControl(isStatusErledigt),
      faelligkeitsDatum: new FormControl(this.data.eintrag?.faelligkeitsDatum),
    });
  }

  private resolveEintragFromForm(): Eintrag | NewEintrag {
    const status =
      this.editEintragForm?.value.isStatusErledigt === true
        ? EintragStatusOptionsEnum.erledigt
        : EintragStatusOptionsEnum.offen;

    return {
      ...(!!this.data.eintrag?.id && { id: this.data.eintrag?.id }),
      beschreibung: this.editEintragForm?.value.beschreibung,
      status,
      faelligkeitsDatum:
        this.editEintragForm?.value.faelligkeitsDatum || new Date(),
    };
  }
}
