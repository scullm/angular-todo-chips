import { createAction, props } from '@ngrx/store';
import { Eintrag, NewEintrag } from '../eintraege.models';

export const loadEintraege = createAction('[Eintraege] Load Eintraege');

export const loadEintraegeSuccess = createAction(
  '[Eintrag] Load Eintrag Success',
  props<{ eintraege: Eintrag[] }>()
);

export const addNewEintrag = createAction(
  '[Eintraege] Add New Eintrag',
  props<{ newEintrag: NewEintrag }>()
);

export const addNewEintragSuccess = createAction(
  '[Eintraege] Add New Eintrag Success',
  props<{ updatedEintraege: Eintrag[] }>()
);

export const editExistingEintrag = createAction(
  '[Eintraege] Edit Existing Eintrag',
  props<{ updatedEintrag: Eintrag }>()
);

export const editExistingEintragSuccess = createAction(
  '[Eintraege] Edit Existing Eintrag Success',
  props<{ updatedEintraege: Eintrag[] }>()
);

export const deleteEintrag = createAction(
  '[Eintraege] Delete Eintrag',
  props<{ eintragId: number }>()
);

export const deleteEintragSuccess = createAction(
  '[Eintrag] Delete Eintrag Success',
  props<{ updatedEintraege: Eintrag[] }>()
);
