import { createFeatureSelector, createSelector } from '@ngrx/store';
import {
  EINTRAEGE_FEATURE_KEY,
  EintraegeState,
  EintraegePartialState,
} from './eintraege.reducer';

export const getEintraegeState = createFeatureSelector<
  EintraegePartialState,
  EintraegeState
>(EINTRAEGE_FEATURE_KEY);

export const getEintraege = createSelector(
  getEintraegeState,
  (state: EintraegeState) => state?.eintraege
);
