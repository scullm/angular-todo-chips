import { Injectable } from '@angular/core';

import { select, Store } from '@ngrx/store';
import { Eintrag, NewEintrag } from '../eintraege.models';

import * as fromEintraegeActions from './eintraege.actions';
import * as fromEintraege from './eintraege.reducer';
import * as EintraegeSelectors from './eintraege.selectors';

@Injectable({ providedIn: 'root' })
export class EintraegeFacade {
  eintraege$ = this.store.pipe(select(EintraegeSelectors.getEintraege));

  constructor(
    private readonly store: Store<fromEintraege.EintraegePartialState>
  ) {}

  loadEintraege() {
    this.store.dispatch(fromEintraegeActions.loadEintraege());
  }

  addNewEintrag(newEintrag: NewEintrag) {
    this.store.dispatch(fromEintraegeActions.addNewEintrag({ newEintrag }));
  }

  editExistingEintrag(updatedEintrag: Eintrag) {
    this.store.dispatch(
      fromEintraegeActions.editExistingEintrag({ updatedEintrag })
    );
  }

  deleteEintrag(eintragId: number) {
    this.store.dispatch(fromEintraegeActions.deleteEintrag({ eintragId }));
  }
}
