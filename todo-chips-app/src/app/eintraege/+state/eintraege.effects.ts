import { Injectable } from '@angular/core';
import { Actions, ofType, Effect } from '@ngrx/effects';

import * as EintraegeActions from './eintraege.actions';

import { mergeMap, map, withLatestFrom } from 'rxjs/operators';
import { from } from 'rxjs';
import { EintraegeService } from '../eintraege.service';
import { Store } from '@ngrx/store';
import {
  EINTRAEGE_FEATURE_KEY,
  EintraegePartialState,
} from './eintraege.reducer';

@Injectable()
export class EintraegeEffects {
  constructor(
    private readonly actions$: Actions,
    private readonly eintraegeService: EintraegeService,
    private store$: Store<EintraegePartialState>
  ) {}

  @Effect() loadEintraege$ = this.actions$.pipe(
    ofType(EintraegeActions.loadEintraege),
    mergeMap(() =>
      from(this.eintraegeService.getEintraege()).pipe(
        map((eintraege) => {
          return EintraegeActions.loadEintraegeSuccess({
            eintraege,
          });
        })
      )
    )
  );

  @Effect() addNewEintrag$ = this.actions$.pipe(
    ofType(EintraegeActions.addNewEintrag),
    withLatestFrom(this.store$),
    mergeMap(([action, store]) =>
      from(
        this.eintraegeService.addNewEintrag(
          action.newEintrag,
          store[EINTRAEGE_FEATURE_KEY].eintraege
        )
      ).pipe(
        map((updatedEintraege) => {
          return EintraegeActions.addNewEintragSuccess({
            updatedEintraege,
          });
        })
      )
    )
  );

  @Effect() editExistingEintrag$ = this.actions$.pipe(
    ofType(EintraegeActions.editExistingEintrag),
    withLatestFrom(this.store$),
    mergeMap(([action, store]) =>
      from(
        this.eintraegeService.editExistingEintrag(
          action.updatedEintrag,
          store[EINTRAEGE_FEATURE_KEY].eintraege
        )
      ).pipe(
        map((updatedEintraege) => {
          return EintraegeActions.editExistingEintragSuccess({
            updatedEintraege,
          });
        })
      )
    )
  );

  @Effect() deleteEintrag$ = this.actions$.pipe(
    ofType(EintraegeActions.deleteEintrag),
    withLatestFrom(this.store$),
    mergeMap(([action, store]) =>
      from(
        this.eintraegeService.deleteEintrag(
          action.eintragId,
          store[EINTRAEGE_FEATURE_KEY].eintraege
        )
      ).pipe(
        map((updatedEintraege) => {
          return EintraegeActions.deleteEintragSuccess({
            updatedEintraege,
          });
        })
      )
    )
  );
}
