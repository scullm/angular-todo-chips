import { createReducer, on, Action } from '@ngrx/store';
import { Eintrag, EintragStatusOptionsEnum } from '../eintraege.models';

import * as EintraegeActions from './eintraege.actions';

export const EINTRAEGE_FEATURE_KEY = 'eintraegeFeatureKey';

export interface EintraegeState {
  eintraege: Eintrag[];
}

export interface EintraegePartialState {
  readonly [EINTRAEGE_FEATURE_KEY]: EintraegeState;
}

export const initialEintraegeState: EintraegeState = {
  eintraege: [],
};

const eintraegeReducer = createReducer(
  initialEintraegeState,
  on(EintraegeActions.loadEintraege, (state) => ({
    ...state,
  })),

  on(EintraegeActions.loadEintraegeSuccess, (state, { eintraege }) => ({
    ...state,
    eintraege,
  })),

  on(EintraegeActions.addNewEintrag, (state) => ({
    ...state,
  })),

  on(EintraegeActions.addNewEintragSuccess, (state, { updatedEintraege }) => ({
    ...state,
    eintraege: updatedEintraege,
  })),

  on(EintraegeActions.editExistingEintrag, (state) => ({
    ...state,
  })),

  on(
    EintraegeActions.editExistingEintragSuccess,
    (state, { updatedEintraege }) => ({
      ...state,
      eintraege: updatedEintraege,
    })
  ),

  on(EintraegeActions.deleteEintrag, (state) => ({
    ...state,
  })),

  on(EintraegeActions.deleteEintragSuccess, (state, { updatedEintraege }) => ({
    ...state,
    eintraege: updatedEintraege,
  }))
);

export function reducer(state: EintraegeState | undefined, action: Action) {
  return eintraegeReducer(state, action);
}
