import { Component } from '@angular/core';
import { EintraegeFacade } from './+state/eintraege.facade';
import {
  EditEintragFormDialogData,
  Eintrag,
  EintragDialogModeOptionsEnum,
  EintragFormDialogData,
} from './eintraege.models';
import { MatDialogRef } from '@angular/material/dialog';
import { EintragDialogFormComponent } from './eintrag-dialog-form/eintrag-dialog-form.component';
import { EintragDialogService } from './eintrag-dialog.service';

@Component({
  selector: 'app-eintraege',
  templateUrl: './eintraege.component.html',
  styleUrls: ['./eintraege.component.scss'],
})
export class EintraegeComponent {
  eintraege$ = this.eintraegeFacade.eintraege$;

  constructor(
    private readonly eintraegeFacade: EintraegeFacade,
    private readonly eintragDialogService: EintragDialogService
  ) {}

  addNewEintrag(): MatDialogRef<EintragDialogFormComponent> {
    const dialogData: EintragFormDialogData = {
      headingText: 'Eintrag bearbeiten',
      dialogMode: EintragDialogModeOptionsEnum.addEintrag,
    };

    return this.eintragDialogService.openDialog(dialogData);
  }

  editExistingEintrag(
    eintrag: Eintrag
  ): MatDialogRef<EintragDialogFormComponent> {
    const dialogData: EditEintragFormDialogData = {
      headingText: 'Eintrag bearbeiten',
      dialogMode: EintragDialogModeOptionsEnum.editEintrag,
      eintrag,
    };

    return this.eintragDialogService.openDialog(dialogData);
  }
}
