import { MatDatepickerIntl } from '@angular/material/datepicker';

export function getGermanDatepickerIntl() {
  const datepickerIntl = new MatDatepickerIntl();
  datepickerIntl.openCalendarLabel = 'Kalendar öffnen';

  return datepickerIntl;
}

export const DATE_PICKER_FORMAT = {
  parse: {
    dateInput: 'dd.MM.yyyy',
  },
  display: {
    dateInput: 'dd.MM.yyyy',
    monthYearLabel: 'MMM yyyy',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM-yyyy',
  },
};
