import { Component, OnInit } from '@angular/core';
import { EintraegeFacade } from './eintraege/+state/eintraege.facade';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
  title = 'todo-chips-app';

  constructor(private readonly eintraegeFacade: EintraegeFacade) {}

  ngOnInit() {
    this.eintraegeFacade.loadEintraege();
  }
}
